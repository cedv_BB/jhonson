// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseEnemy.h"
#include "EngineUtils.h"
#include "Engine/TargetPoint.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"


// Sets default values
ABaseEnemy::ABaseEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	OnActorHit.AddDynamic(this, &ABaseEnemy::OnHit);
	auto FirstPersonProjectileBPClass = ConstructorHelpers::FClassFinder<AActor>(TEXT("/Game/Blueprints/FirstPersonProjectile"));
	if (FirstPersonProjectileBPClass.Succeeded()) {
		ProjectileClass = FirstPersonProjectileBPClass.Class;
	}
}

// Called when the game starts or when spawned
void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

TArray<ATargetPoint*> ABaseEnemy::GetWaypoints()
{
	return waypoints;
}

bool ABaseEnemy::GetAtack()
{
	return atack;
}

void ABaseEnemy::SetAtack(bool atackChange)
{
	this->atack = atackChange;
}

bool ABaseEnemy::GetType()
{
	return false;
}

void ABaseEnemy::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor) {
		if (OtherActor->IsA(ProjectileClass)) {
			Destroy();
		}
	}
}