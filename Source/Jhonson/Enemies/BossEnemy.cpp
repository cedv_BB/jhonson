// Fill out your copyright notice in the Description page of Project Settings.

#include "BossEnemy.h"
#include "EngineUtils.h"
#include "Engine/TargetPoint.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"

// Sets default values
ABossEnemy::ABossEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	life = 10;

	OnActorHit.AddDynamic(this, &ABossEnemy::OnHit);
	auto FirstPersonProjectileBPClass = ConstructorHelpers::FClassFinder<AActor>(TEXT("/Game/Blueprints/FirstPersonProjectile"));
	if (FirstPersonProjectileBPClass.Succeeded()) {
		ProjectileClass = FirstPersonProjectileBPClass.Class;
	}
}

// Called when the game starts or when spawned
void ABossEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABossEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABossEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


TArray<ATargetPoint*> ABossEnemy::GetWaypoints()
{
	return waypoints;
}

bool ABossEnemy::GetAtack()
{
	return atack;
}

void ABossEnemy::SetAtack(bool atackChange)
{
	this->atack = atackChange;
}

int ABossEnemy::GetLife()
{
	return life;
}

bool ABossEnemy::GetType()
{
	return true;
}

void ABossEnemy::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor) {
		if (OtherActor->IsA(ProjectileClass)) {
			life--;
		}
	}
}