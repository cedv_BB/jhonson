// Fill out your copyright notice in the Description page of Project Settings.

#include "ShootTask.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Interfaces/EnemyInterface.h"
#include "AIController.h"



EBTNodeResult::Type UShootTask::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	AAIController* aiController = Cast<AAIController>(OwnerComp.GetAIOwner());
	if (!aiController) {
		return EBTNodeResult::Type::Failed;
	}
	IEnemyInterface* enemy = Cast<IEnemyInterface>(aiController->GetPawn());
	if (!enemy) {
		return EBTNodeResult::Type::Failed;
	}
	if (enemy->GetType()) {
		GetWorld()->SpawnActor<AActor>(proyectile.Get(), OwnerComp.GetOwner()->GetActorLocation(), OwnerComp.GetOwner()->GetActorRotation());
	}
	

	return EBTNodeResult::Type::Succeeded;
}
