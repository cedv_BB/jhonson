// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "JhonsonGameMode.h"
#include "JhonsonHUD.h"
#include "JhonsonCharacter.h"
#include "UObject/ConstructorHelpers.h"

AJhonsonGameMode::AJhonsonGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AJhonsonHUD::StaticClass();
}
